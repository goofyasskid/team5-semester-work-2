def bubble_sort(lst: list) -> list:
    index = len(lst) - 1
    is_sorted = False

    while not is_sorted:
        is_sorted = True
        for idx in range(index):
            if lst[idx] > lst[idx+1]:
                is_sorted = False
                lst[idx], lst[idx+1] = lst[idx+1], lst[idx]
    return lst
