import pandas as pd
import matplotlib.pyplot as plt
import os


def create_dir(  ):
    try:
        os.mkdir(path='load_testing_plots')

    except OSError:
        print('Path is already exists')



def create_chart(csv_file_path: str):
    data = pd.read_csv(csv_file_path)
    data_sorted = data.sort_values(["size"])
    df = data_sorted.plot(x='size')
    df.set_xlabel('Size')
    df.set_ylabel('Time')
    plt.savefig('load_testing_plots/bubble_sort.jpg')
    plt.show()

def main():
    csv_file_path = input()
    create_dir()
    create_chart(csv_file_path)


if __name__ == '__main__':
    main()
#C:/Users/123/Projects/team5-semester-work-2/load_testing_measurements/1621798364.4431314-bubble_sort.csv
