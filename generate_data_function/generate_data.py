import os
import random


def create_path_if_not_exists():
    path_name = 'load_testing_data'
    if os.path.exists(path_name):
        print('dir is also created')
        exit()
    else:
        try:
            os.mkdir(path_name)
        except OSError:
            print('Directory has also been created')


def create_random_files(start, end, step, count):
    for size in range(start, end, step):
        for file_number in range(1, count+1):
            with open(os.path.join('load_testing_data', f'{size}_{file_number}.txt'), mode='w') as f:
                random_data = [random.randint(-1000, 1000) for _ in range(size)]
                f.write(' '.join(str(i) for i in random_data))
        

def generate_data(start: int, end: int, step: int, count: int) -> None:
    create_path_if_not_exists()
    create_random_files(start, end, step, count)


generate_data(50, 1000, 25, 5)